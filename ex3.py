import psycopg2


def ex3():
    print("EXERCICI 3\n")
    PSQL_HOST = "localhost"
    PSQL_PORT = "5432"
    PSQL_USER = "root"
    PSQL_PASS = "root"
    PSQL_DB = "Bddemo"
    result = None

    try:
        connection_address = """
        host=%s port =%s user=%s password=%s dbname=%s
        """ % (PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASS, PSQL_DB)

        connection = psycopg2.connect(connection_address)

        cursor = connection.cursor()
        select = "SELECT id FROM cliente;"
        cursor.execute(select)

        for record in cursor:
            print(record)

        option = input("De quin client vols saber les dades?\n")
        selectOption = "SELECT * from cliente where id ='%s';" % option
        cursor.execute(selectOption)

        for record in cursor:
            print(record)

        result = cursor.statusmessage
        cursor.close()
    except(Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        result = error
    finally:
        connection.close()

    print(result)

ex3()
