CREATE TABLE IF NOT EXISTS cliente (
    dni varchar(9) PRIMARY KEY,
    name varchar(10)
    );

INSERT INTO cliente(dni, name) VALUES
("41111111K","Daniel"),
("31111111L","Andrii"),
("21111111M","Ronny"),
("51111111N","Andreu"),
("11111111O","Ginés");