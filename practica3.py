import psycopg2


def ex1():
    print("EXERCICI 1\n")
    PSQL_HOST = "localhost"
    PSQL_PORT = "5432"
    PSQL_USER = "root"
    PSQL_PASS = "root"
    PSQL_DB = "Bddemo"
    result = None

    try:
        connection_address = """
        host=%s port =%s user=%s password=%s dbname=%s
        """ % (PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASS, PSQL_DB)

        connection = psycopg2.connect(connection_address)

        cursor = connection.cursor()
        SQL = "SELECT * FROM res_users;"
        cursor.execute(SQL)

        all_values = cursor.fetchall()

        cursor.close()
    except(Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        result = error
    finally:
        connection.close()

    print(result)
    print('Get values: ', all_values, "\n")


def ex2():
    print("EXERCICI 2\n")
    PSQL_HOST = "localhost"
    PSQL_PORT = "5432"
    PSQL_USER = "root"
    PSQL_PASS = "root"
    PSQL_DB = "Bddemo"
    result = None

    try:
        connection_address = """
        host=%s port =%s user=%s password=%s dbname=%s
        """ % (PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASS, PSQL_DB)

        connection = psycopg2.connect(connection_address)

        cursor = connection.cursor()
        drop = "DROP TABLE IF EXISTS cliente;"
        create = "CREATE TABLE IF NOT EXISTS cliente (id varchar(9) PRIMARY KEY,name varchar(10));"
        insert = "INSERT INTO cliente(id, name) VALUES ('1','Daniel'), ('2','Andrii'), " \
                 "('3','Ronny'), ('4','Andreu'),('5','Ginés');"

        cursor.execute(drop)
        cursor.execute(create)
        cursor.execute(insert)

        result = cursor.statusmessage
        cursor.close()
        connection.commit()

    except(Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        result = error
    finally:
        connection.close()
    print(result, "\n")


def ex3():
    print("EXERCICI 3\n")
    PSQL_HOST = "localhost"
    PSQL_PORT = "5432"
    PSQL_USER = "root"
    PSQL_PASS = "root"
    PSQL_DB = "Bddemo"
    result = None

    try:
        connection_address = """
        host=%s port =%s user=%s password=%s dbname=%s
        """ % (PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASS, PSQL_DB)

        connection = psycopg2.connect(connection_address)

        cursor = connection.cursor()
        select = "SELECT id FROM cliente;"
        cursor.execute(select)

        for record in cursor:
            print(record)

        option = input("De quin client vols saber les dades?\n")
        selectOption = "SELECT * from cliente where id ='%s';" % option
        cursor.execute(selectOption)

        for record in cursor:
            print(record)

        result = cursor.statusmessage
        cursor.close()
    except(Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        result = error
    finally:
        connection.close()

    print(result, "\n")


def ex4():
    print("EXERCICI 4\n")

    PSQL_HOST = "localhost"
    PSQL_PORT = "5432"
    PSQL_USER = "root"
    PSQL_PASS = "root"
    PSQL_DB = "Bddemo"
    result = None

    try:
        connection_address = """
        host=%s port =%s user=%s password=%s dbname=%s
        """ % (PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASS, PSQL_DB)

        connection = psycopg2.connect(connection_address)

        cursor = connection.cursor()
        select = "SELECT id FROM cliente;"
        cursor.execute(select)

        for record in cursor:
            print(record)


        option = input("De quin client vols actualitzar les dades?\n")
        selectOption = "SELECT * from cliente where id ='%s';" % option
        cursor.execute(selectOption)

        for record in cursor:
            print(record)

        newName = input("Quin és el nou nom amb el que vols actualitzar el client?\n")
        update = "UPDATE cliente SET name ='%s' WHERE id ='%s';" % (newName, option)

        cursor.execute(update)
        result = cursor.statusmessage
        cursor.close()
        connection.commit()

    except(Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        result = error
    finally:
        connection.close()

    print(result, "\n")

ex1()
ex2()
ex3()
ex4()
