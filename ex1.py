import psycopg2

def ex1():
    print("EXERCICI 1\n")
    PSQL_HOST = "localhost"
    PSQL_PORT = "5432"
    PSQL_USER = "root"
    PSQL_PASS = "root"
    PSQL_DB = "Bddemo"
    result = None

    try:
        connection_address = """
        host=%s port =%s user=%s password=%s dbname=%s
        """ % (PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASS, PSQL_DB)

        connection = psycopg2.connect(connection_address)

        cursor = connection.cursor()
        SQL = "SELECT * FROM res_users;"
        cursor.execute(SQL)

        all_values = cursor.fetchall()

        cursor.close()
    except(Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        result = error
    finally:
        connection.close()

    print(result)
    print('Get values: ', all_values)


ex1()


