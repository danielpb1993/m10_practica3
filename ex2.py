import psycopg2


def ex2():
    print("EXERCICI 2\n")
    PSQL_HOST = "localhost"
    PSQL_PORT = "5432"
    PSQL_USER = "root"
    PSQL_PASS = "root"
    PSQL_DB = "Bddemo"
    result = None

    try:
        connection_address = """
        host=%s port =%s user=%s password=%s dbname=%s
        """ % (PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASS, PSQL_DB)

        connection = psycopg2.connect(connection_address)

        cursor = connection.cursor()
        drop = "DROP TABLE IF EXISTS cliente;"
        create = "CREATE TABLE IF NOT EXISTS cliente (id varchar(9) PRIMARY KEY,name varchar(10));"
        insert = "INSERT INTO cliente(id, name) VALUES ('1','Daniel'), ('2','Andrii'), " \
                 "('3','Ronny'), ('4','Andreu'),('5','Ginés');"

        cursor.execute(drop)
        cursor.execute(create)
        cursor.execute(insert)

        result = cursor.statusmessage
        cursor.close()
        connection.commit()

    except(Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        result = error
    finally:

        connection.close()
    print(result)


ex2()
